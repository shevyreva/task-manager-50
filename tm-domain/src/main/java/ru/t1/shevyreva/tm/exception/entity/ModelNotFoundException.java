package ru.t1.shevyreva.tm.exception.entity;

public class ModelNotFoundException extends AbstractEntityException {

    public ModelNotFoundException() {
        super("Error! Object not found.");
    }

}
