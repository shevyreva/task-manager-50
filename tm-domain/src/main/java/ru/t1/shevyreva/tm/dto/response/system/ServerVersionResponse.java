package ru.t1.shevyreva.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;

@Getter
@Setter
public class ServerVersionResponse extends AbstractResponse {

    @NotNull
    private String version;

}
