package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.dto.IUserDtoService;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.field.EmailEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.LoginEmptyException;
import ru.t1.shevyreva.tm.exception.field.PasswordEmptyException;
import ru.t1.shevyreva.tm.exception.user.ExistsLoginException;
import ru.t1.shevyreva.tm.service.dto.UserDtoService;

public class UserServiceTest {

    private static final int NUMBER_OF_USER = 2;
    @NotNull
    private static IPropertyService propertyService;
    @NotNull
    private static IConnectionService connectionService;
    @NotNull
    private IUserDtoService userService;

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void closeConnection() {
        connectionService.getEntityManager().close();
    }

    @Before
    public void initService() {
        System.out.println("BEFORE");
        userService = new UserDtoService(connectionService, propertyService);
        for (int i = 1; i <= NUMBER_OF_USER; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("user " + i);
            user.setEmail("email " + i);
            user.setPasswordHash("password" + i);
            userService.add(user);
        }
    }

    @Test
    public void testCreate() {
        final int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userService.getSize());
        @NotNull final UserDTO user = userService.create("newLogin", "newPassword", "@email");

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, "password"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("new", null));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user 1", "password"));
        Assert.assertEquals(expectedCount, userService.getSize());
    }

    @Test
    public void updateUser() {
        @NotNull final String newFirstName = "FirstName";
        @NotNull final String newLastName = "LastName";
        @NotNull final String newMiddleName = "MiddleName";
        @NotNull final UserDTO user = userService.findByLogin("user 1");
        @NotNull final String userId = user.getId();

        userService.updateUser(userId, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(newFirstName, userService.findByLogin("user 1").getFirstName());
        Assert.assertEquals(newMiddleName, userService.findByLogin("user 1").getMiddleName());
        Assert.assertEquals(newLastName, userService.findByLogin("user 1").getLastName());
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(null, newFirstName, newMiddleName, newLastName));
    }

    @Test
    public void testFindByLogin() {
        @NotNull final String login = "user 1";
        @NotNull final String email = "email 1";
        @NotNull final String incorrectLogin = "user 555";
        @NotNull final UserDTO user = userService.findByLogin(login);

        Assert.assertNotNull(user);
        Assert.assertEquals(email, userService.findByLogin(login).getEmail());
        Assert.assertEquals(login, userService.findByLogin(login).getLogin());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertNull(userService.findByLogin(incorrectLogin));
    }

    @Test
    public void testFindByEmail() {
        @NotNull final String login = "user 1";
        @NotNull final String email = "email 1";
        @NotNull final String incorrectEmail = "email 555";
        @NotNull final UserDTO user = userService.findByEmail(email);

        Assert.assertNotNull(user);
        Assert.assertEquals(email, user.getEmail());
        Assert.assertEquals(login, user.getLogin());
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));
        Assert.assertNull(userService.findByEmail(incorrectEmail));
    }

    @Test
    public void testLockUser() {
        @NotNull final UserDTO newUser = new UserDTO();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        newUser.setPasswordHash("12345");
        userService.add(newUser);

        userService.lockUser(login);
        Assert.assertTrue(userService.findByLogin(login).getLocked());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUser(null));
    }

    @Test
    public void testUnlockUser() {
        @NotNull final UserDTO newUser = new UserDTO();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        newUser.setPasswordHash("12345");
        userService.add(newUser);

        userService.unlockUser(login);
        Assert.assertFalse(userService.findByLogin(login).getLocked());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUser(null));
    }

    @Test
    public void testRemoveOne() {
        int expectedCount = NUMBER_OF_USER - 1;
        Assert.assertEquals(NUMBER_OF_USER, userService.getSize());

        @NotNull final UserDTO userForRemove = userService.findByLogin("user 1");
        @NotNull final String login = userForRemove.getLogin();
        @NotNull final String email = userForRemove.getEmail();
        @NotNull final String userId = userForRemove.getId();
        userService.removeOneById(userId);

        Assert.assertFalse(userService.isLoginExist(login));
        Assert.assertFalse(userService.isEmailExist(email));
        Assert.assertEquals(expectedCount, userService.getSize());
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.findOneById(userId));
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeOneById(null));
    }

    @After
    public void removeAll() {
        userService.removeAll();
    }

}
