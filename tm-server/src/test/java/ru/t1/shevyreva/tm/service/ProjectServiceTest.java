package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.dto.IProjectDtoService;
import ru.t1.shevyreva.tm.api.service.dto.IUserDtoService;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.enumerated.ProjectDtoSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.NameEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.service.dto.ProjectDtoService;
import ru.t1.shevyreva.tm.service.dto.UserDtoService;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private IProjectDtoService projectService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private String testId;

    @NotNull
    private String adminId;

    @NotNull
    private List<ProjectDTO> projectList = new ArrayList<>();

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void closeConnection() {
        connectionService.getEntityManager().close();
    }

    @Before
    public void init() {
        System.out.println("BEFORE");
        userService = new UserDtoService(connectionService, propertyService);
        projectService = new ProjectDtoService(connectionService);

        @NotNull final UserDTO userTest = new UserDTO("testUser", "test", "test@mail.ru");
        testId = userTest.getId();
        @NotNull final UserDTO userAdmin = new UserDTO("testAdmin", "admin", "admin@mail.ru");
        adminId = userAdmin.getId();
        userService.add(userTest);
        userService.add(userAdmin);

        @NotNull final ProjectDTO projectTestOne = new ProjectDTO("project 1", Status.NON_STARTED);
        projectTestOne.setUserId(testId);
        @NotNull final ProjectDTO projectTestTwo = new ProjectDTO("project 2", Status.NON_STARTED);
        projectTestTwo.setUserId(testId);
        @NotNull final ProjectDTO projectAdminOne = new ProjectDTO("project 1", Status.NON_STARTED);
        projectAdminOne.setUserId(adminId);
        @NotNull final ProjectDTO projectAdminTwo = new ProjectDTO("project 2", Status.NON_STARTED);
        projectAdminTwo.setUserId(adminId);
        projectList.add(projectTestOne);
        projectList.add(projectTestTwo);
        projectList.add(projectAdminOne);
        projectList.add(projectAdminTwo);
        projectService.add(projectList);
    }

    @Test
    public void addProject() {
        Assert.assertEquals(2, projectService.getSize(testId));
        @NotNull final ProjectDTO newProject = new ProjectDTO("project 3", Status.NON_STARTED);
        newProject.setUserId(testId);
        projectService.add(newProject);
        Assert.assertEquals(3, projectService.getSize(testId));
    }

    @Test
    public void addProjects() {
        Assert.assertEquals(2, projectService.getSize(testId));
        @NotNull final ProjectDTO newProjectOne = new ProjectDTO("project 3", Status.NON_STARTED);
        newProjectOne.setUserId(testId);
        @NotNull final ProjectDTO newProjectTwo = new ProjectDTO("project 4", Status.NON_STARTED);
        newProjectTwo.setUserId(testId);
        @NotNull final List<ProjectDTO> projects = new ArrayList<>();
        projects.add(newProjectOne);
        projects.add(newProjectTwo);
        projectService.add(projects);
        Assert.assertEquals(4, projectService.getSize(testId));
    }

    @Test
    public void createProject() {
        Assert.assertEquals(2, projectService.getSize(testId));
        projectService.create(testId, "project 3");
        Assert.assertEquals(3, projectService.getSize(testId));

        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(testId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, "project"));
    }

    @Test
    public void findByProjectId() {
        @NotNull final ProjectDTO newProject = new ProjectDTO();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final String projectId = newProject.getId();
        @NotNull final String incorrectProjectId = UUID.randomUUID().toString();

        newProject.setUserId(testId);
        newProject.setName("newProject");
        newProject.setDescription("newDescription");
        projectService.add(newProject);
        Assert.assertNotNull(projectService.findOneById(testId, projectId));

        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(null, projectId));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(testId, null));
        Assert.assertThrows(NoResultException.class, () -> projectService.findOneById(incorrectUserId, projectId));
        Assert.assertThrows(NoResultException.class, () -> projectService.findOneById(testId, incorrectProjectId));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(4, projectService.findAll().size());
        Assert.assertNotEquals(5, projectService.findAll().size());
    }

    @Test
    public void findAllForUser() {
        final int userCount = 2;
        Assert.assertEquals(userCount, projectService.findAll(testId, ProjectDtoSort.BY_CREATED).size());
        Assert.assertEquals(userCount, projectService.findAll(testId, ProjectDtoSort.BY_NAME).size());
        Assert.assertEquals(userCount, projectService.findAll(testId, ProjectDtoSort.BY_STATUS).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(null, null));
    }

    @Test
    public void removeOne() {
        Assert.assertEquals(2, projectService.getSize(testId));
        long expectedCount = 1;

        @NotNull final ProjectDTO removeProject = projectService.findOneByIndex(testId, 0);
        @NotNull final String id = removeProject.getId();
        projectService.removeOne(testId, removeProject);

        Assert.assertThrows(NoResultException.class, () -> projectService.findOneById(testId, id));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOne(testId, null));

        Assert.assertEquals(expectedCount, projectService.getSize(testId));
    }

    @Test
    public void removeOneById() {
        Assert.assertEquals(2, projectService.getSize(testId));
        long expectedCount = 1;

        @NotNull final ProjectDTO removeProject = projectService.findOneByIndex(testId, 0);
        @NotNull final String id = removeProject.getId();
        projectService.removeOneById(testId, id);

        Assert.assertThrows(NoResultException.class, () -> projectService.findOneById(testId, id));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOne(testId, null));

        Assert.assertEquals(expectedCount, projectService.getSize(testId));
    }

    @Test
    public void updateById() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newDescription";
        @NotNull final ProjectDTO updateProject = projectService.findOneByIndex(testId, 0);
        @NotNull final String id = updateProject.getId();

        projectService.updateById(testId, id, newName, newDescription);
        Assert.assertEquals(newName, projectService.findOneById(testId, id).getName());
        Assert.assertEquals(newDescription, projectService.findOneById(testId, id).getDescription());

        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(testId, null, newName, newDescription));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, id, newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(testId, id, null, newDescription));
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final ProjectDTO updateProject = projectService.findOneByIndex(testId, 0);
        @NotNull final String id = updateProject.getId();
        @NotNull final Status completed = Status.COMPLETED;
        @NotNull final Status progress = Status.IN_PROGRESS;
        @NotNull final Status notStarted = Status.NON_STARTED;

        projectService.changeProjectStatusById(testId, id, completed);
        Assert.assertEquals(completed, projectService.findOneById(testId, id).getStatus());

        projectService.changeProjectStatusById(testId, id, progress);
        Assert.assertEquals(progress, projectService.findOneById(testId, id).getStatus());

        projectService.changeProjectStatusById(testId, id, notStarted);
        Assert.assertEquals(notStarted, projectService.findOneById(testId, id).getStatus());

        Assert.assertThrows(StatusNotFoundException.class, () -> projectService.changeProjectStatusById(testId, id, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(testId, null, completed));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, id, completed));
    }

    @After
    public void removeAll() {
        projectService.removeAll();
    }

}
