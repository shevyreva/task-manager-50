package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.dto.ISessionDtoService;
import ru.t1.shevyreva.tm.api.service.dto.IUserDtoService;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.service.dto.SessionDtoService;
import ru.t1.shevyreva.tm.service.dto.UserDtoService;

public class SessionServiceTest {

    private static final int NUMBER_OF_USER = 2;

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private static ISessionDtoService sessionService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private String testId;

    @NotNull
    private String adminId;

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        sessionService = new SessionDtoService(connectionService);
    }

    @AfterClass
    public static void closeConnection() {
        connectionService.getEntityManager().close();
    }

    @Before
    public void initService() {
        System.out.println("BEFORE");
        userService = new UserDtoService(connectionService, propertyService);
        for (int i = 1; i <= NUMBER_OF_USER; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("user " + i);
            user.setEmail("email " + i);
            user.setPasswordHash("password" + i);
            if (i == 1) {
                user.setRole(Role.USUAL);
                testId = user.getId();
            } else {
                user.setRole(Role.ADMIN);
                adminId = user.getId();
            }
            userService.add(user);
        }
    }

    @Test
    public void add() {
        @NotNull final SessionDTO sessionOne = new SessionDTO();
        sessionOne.setUserId(testId);
        sessionOne.setRole(userService.findOneById(testId).getRole());
        sessionService.add(sessionOne);

        @NotNull final SessionDTO sessionTwo = new SessionDTO();
        sessionTwo.setUserId(adminId);
        sessionTwo.setRole(userService.findOneById(adminId).getRole());
        sessionService.add(sessionTwo);

        Assert.assertThrows(Exception.class, () -> sessionService.add(null));
    }

    @Test
    public void findAll() {
        @NotNull final SessionDTO sessionOne = new SessionDTO();
        sessionOne.setUserId(testId);
        sessionOne.setRole(userService.findOneById(testId).getRole());
        sessionService.add(sessionOne);

        @NotNull final SessionDTO sessionTwo = new SessionDTO();
        sessionTwo.setUserId(adminId);
        sessionTwo.setRole(userService.findOneById(adminId).getRole());
        sessionService.add(sessionTwo);

        sessionService.findAll();
        Assert.assertEquals(2, sessionService.getSize());
    }

    @Test
    public void removeOne() {
        @NotNull final SessionDTO sessionOne = new SessionDTO();
        sessionOne.setUserId(testId);
        sessionOne.setRole(userService.findOneById(testId).getRole());
        sessionService.add(sessionOne);

        @NotNull final SessionDTO sessionTwo = new SessionDTO();
        sessionTwo.setUserId(adminId);
        sessionTwo.setRole(userService.findOneById(adminId).getRole());
        sessionService.add(sessionTwo);

        sessionService.removeOne(sessionOne);
        Assert.assertEquals(1, sessionService.getSize());
    }

    @After
    public void removeAll() {
        sessionService.removeAll();
    }

}
