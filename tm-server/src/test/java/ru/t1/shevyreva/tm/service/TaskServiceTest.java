package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.dto.ITaskDtoService;
import ru.t1.shevyreva.tm.api.service.dto.IUserDtoService;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskDtoSort;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.NameEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.service.dto.TaskDtoService;
import ru.t1.shevyreva.tm.service.dto.UserDtoService;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private ITaskDtoService taskService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private String testId;

    @NotNull
    private String adminId;

    @NotNull
    private List<TaskDTO> taskList = new ArrayList<>();

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void closeConnection() {
        connectionService.getEntityManager().close();
    }

    @Before
    public void init() {
        System.out.println("BEFORE");
        userService = new UserDtoService(connectionService, propertyService);
        taskService = new TaskDtoService(connectionService);

        @NotNull final UserDTO userTest = new UserDTO("testUser", "test", "test@mail.ru");
        testId = userTest.getId();
        @NotNull final UserDTO userAdmin = new UserDTO("testAdmin", "admin", "admin@mail.ru");
        adminId = userAdmin.getId();
        userService.add(userTest);
        userService.add(userAdmin);

        @NotNull final TaskDTO taskTestOne = new TaskDTO("task 1", Status.NON_STARTED);
        taskTestOne.setUserId(testId);
        @NotNull final TaskDTO taskTestTwo = new TaskDTO("task 2", Status.NON_STARTED);
        taskTestTwo.setUserId(testId);
        @NotNull final TaskDTO taskAdminOne = new TaskDTO("task 1", Status.NON_STARTED);
        taskAdminOne.setUserId(adminId);
        @NotNull final TaskDTO taskAdminTwo = new TaskDTO("task 2", Status.NON_STARTED);
        taskAdminTwo.setUserId(adminId);
        taskList.add(taskTestOne);
        taskList.add(taskTestTwo);
        taskList.add(taskAdminOne);
        taskList.add(taskAdminTwo);
        taskService.add(taskList);
    }

    /*
        @After
        public void clearTasks() {
            System.out.println("AFTER");
            taskService.removeAll();
            Assert.assertEquals(0, taskService.getSize(testId));
        }
    */
    @Test
    public void addTask() {
        Assert.assertEquals(2, taskService.getSize(testId));
        @NotNull final TaskDTO newTask = new TaskDTO("task 3", Status.NON_STARTED);
        newTask.setUserId(testId);
        taskService.add(newTask);
        Assert.assertEquals(3, taskService.getSize(testId));
    }

    @Test
    public void addTasks() {
        Assert.assertEquals(2, taskService.getSize(testId));
        @NotNull final TaskDTO newTaskOne = new TaskDTO("task 3", Status.NON_STARTED);
        newTaskOne.setUserId(testId);
        @NotNull final TaskDTO newTaskTwo = new TaskDTO("task 4", Status.NON_STARTED);
        newTaskTwo.setUserId(testId);
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        tasks.add(newTaskOne);
        tasks.add(newTaskTwo);
        taskService.add(tasks);
        Assert.assertEquals(4, taskService.getSize(testId));
    }

    @Test
    public void createTask() {
        Assert.assertEquals(2, taskService.getSize(testId));
        taskService.create(testId, "task 3");
        Assert.assertEquals(3, taskService.getSize(testId));

        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(testId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(null, "task"));
    }

    @Test
    public void findByTaskId() {
        @NotNull final TaskDTO newTask = new TaskDTO();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final String taskId = newTask.getId();
        @NotNull final String incorrectTaskId = UUID.randomUUID().toString();

        newTask.setUserId(testId);
        newTask.setName("newTask");
        newTask.setDescription("newDescription");
        taskService.add(newTask);
        Assert.assertNotNull(taskService.findOneById(testId, taskId));

        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(null, taskId));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(testId, null));
        Assert.assertThrows(NoResultException.class, () -> taskService.findOneById(incorrectUserId, taskId));
        Assert.assertThrows(NoResultException.class, () -> taskService.findOneById(testId, incorrectTaskId));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(4, taskService.findAll().size());
        Assert.assertNotEquals(5, taskService.findAll().size());
    }

    @Test
    public void findAllForUser() {
        final int userCount = 2;
        Assert.assertEquals(userCount, taskService.findAll(testId, TaskDtoSort.BY_CREATED).size());
        Assert.assertEquals(userCount, taskService.findAll(testId, TaskDtoSort.BY_NAME).size());
        Assert.assertEquals(userCount, taskService.findAll(testId, TaskDtoSort.BY_STATUS).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(null, null));
    }

    @Test
    public void removeOne() {
        Assert.assertEquals(2, taskService.getSize(testId));
        long expectedCount = 1;

        @NotNull final TaskDTO removeTask = taskService.findOneByIndex(testId, 0);
        @NotNull final String id = removeTask.getId();
        taskService.removeOne(testId, removeTask);

        Assert.assertThrows(NoResultException.class, () -> taskService.findOneById(testId, id));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOne(testId, null));

        Assert.assertEquals(expectedCount, taskService.getSize(testId));
    }

    @Test
    public void removeOneById() {
        Assert.assertEquals(2, taskService.getSize(testId));
        long expectedCount = 1;

        @NotNull final TaskDTO removeTask = taskService.findOneByIndex(testId, 0);
        @NotNull final String id = removeTask.getId();
        taskService.removeOneById(testId, id);

        Assert.assertThrows(NoResultException.class, () -> taskService.findOneById(testId, id));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOne(testId, null));

        Assert.assertEquals(expectedCount, taskService.getSize(testId));
    }

    @Test
    public void updateById() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newDescription";
        @NotNull final TaskDTO updateTask = taskService.findOneByIndex(testId, 0);
        @NotNull final String id = updateTask.getId();

        taskService.updateById(testId, id, newName, newDescription);
        Assert.assertEquals(newName, taskService.findOneById(testId, id).getName());
        Assert.assertEquals(newDescription, taskService.findOneById(testId, id).getDescription());

        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(testId, null, newName, newDescription));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, id, newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(testId, id, null, newDescription));
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final TaskDTO updateTask = taskService.findOneByIndex(testId, 0);
        @NotNull final String id = updateTask.getId();
        @NotNull final Status completed = Status.COMPLETED;
        @NotNull final Status progress = Status.IN_PROGRESS;
        @NotNull final Status notStarted = Status.NON_STARTED;

        taskService.changeTaskStatusById(testId, id, completed);
        Assert.assertEquals(completed, taskService.findOneById(testId, id).getStatus());

        taskService.changeTaskStatusById(testId, id, progress);
        Assert.assertEquals(progress, taskService.findOneById(testId, id).getStatus());

        taskService.changeTaskStatusById(testId, id, notStarted);
        Assert.assertEquals(notStarted, taskService.findOneById(testId, id).getStatus());

        Assert.assertThrows(StatusNotFoundException.class, () -> taskService.changeTaskStatusById(testId, id, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(testId, null, completed));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, id, completed));
    }

    @After
    public void removeAll() {
        taskService.removeAll();
    }

}
