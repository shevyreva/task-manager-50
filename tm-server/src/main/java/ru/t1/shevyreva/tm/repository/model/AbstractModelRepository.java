package ru.t1.shevyreva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.DBConstants;
import ru.t1.shevyreva.tm.api.repository.model.IAbstractModelRepository;
import ru.t1.shevyreva.tm.comporator.DateComparator;
import ru.t1.shevyreva.tm.comporator.StatusComparator;
import ru.t1.shevyreva.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractModelRepository<M extends AbstractModel> implements IAbstractModelRepository<M> {


    @NotNull
    protected final EntityManager entityManager;

    public AbstractModelRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == DateComparator.INSTANCE) return DBConstants.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return DBConstants.COLUMN_STATUS;
        else return DBConstants.COLUMN_NAME;
    }

    @Override
    public void add(@NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
