package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.system.ServerAboutRequest;
import ru.t1.shevyreva.tm.dto.response.system.ServerAboutResponse;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String DESCRIPTION = "Show about program.";

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @NotNull ServerAboutResponse response = getSystemEndpoint().getAbout(request);
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

}
